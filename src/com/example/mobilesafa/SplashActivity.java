package com.example.mobilesafa;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.mobilesafa.utils.StreamTools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.EventLogTags.Description;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends Activity {

	protected static final int ENTER_HOME = 0;
	protected static final int SHOW_UPDATE_DIALOG = 1;
	protected static final int URL_ERROR = 2;
	protected static final int NETWORK_ERROR = 3;
	protected static final int JSON_ERROR = 4;
	protected static final String TAG = "SplashActivity";
	private String version;
	private String description;
	private String apkurl;
	private TextView tv_splash_version;
	
	private SharedPreferences sp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		sp = getSharedPreferences("config", MODE_PRIVATE);
		tv_splash_version = (TextView) findViewById(R.id.tv_splash_version);
		tv_splash_version.setText(getVersionName());
		boolean update = sp.getBoolean("update", false);
		if(update){
			//检测自动升级
			checkUpdate();
		}else{
			//自动升级已经关闭了
			handler.postDelayed(new Runnable(){

				@Override
				public void run() {
					// 进入主页面
					enterHome();
				}
				
			}, 2000);
		}
		
		// 给启动页面添加 动画
		AlphaAnimation aa = new AlphaAnimation(0.3f, 1.0f);
		aa.setDuration(4000);
		findViewById(R.id.rl_root_splash).startAnimation(aa);
		
		tv_update_info = (TextView) findViewById(R.id.tv_update_info);
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch (msg.what) {
			case SHOW_UPDATE_DIALOG:// 显示升级的对话框
				Log.i(TAG, "显示升级的对话框");
				showUpdateDialog();
				break;
			case ENTER_HOME:// 进入主页面
				enterHome();
				break;

			case URL_ERROR:// URL错误
				enterHome();
				Toast.makeText(getApplicationContext(), "URL错误", 0).show();

				break;

			case NETWORK_ERROR:// 网络异常
				enterHome();
				Toast.makeText(SplashActivity.this, "网络异常", 0).show();
				break;

			case JSON_ERROR:// JSON解析出错
				enterHome();
				Toast.makeText(SplashActivity.this, "JSON解析出错", 0).show();
				break;

			default:
				break;
			}
		}

	};
	private TextView tv_update_info;

	private void checkUpdate() {

		new Thread() {
			

			public void run() {
				// URLhttp://192.168.1.111:8080/updateinfo.html

				Message mes = Message.obtain();// 定义传递的消息
				long startTime = System.currentTimeMillis();// 记录起始时间 用于记录延时
				try {
					//R.string.serverurl 在 res/values/string.xml 中添加
					URL url = new URL(getString(R.string.serverurl));
					// 联网
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(4000);
					int code = conn.getResponseCode();
					if (code == 200) {
						// 联网成功
						InputStream is = conn.getInputStream();
						// 把流转成String
						String result = StreamTools.readFromStream(is);
						Log.i(TAG, "联网成功了" + result);
						// json解析
						JSONObject obj = new JSONObject(result);
						version = (String) obj.get("version");

						description = (String) obj.get("description");
						apkurl = (String) obj.get("apkurl");

						// 校验是否有新版本
						if (getVersionName().equals(version)) {
							// 版本一致，没有新版本，进入主页面
							mes.what = ENTER_HOME;
						} else {
							// 有新版本，弹出一升级对话框
							mes.what = SHOW_UPDATE_DIALOG;

						}

					}

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					mes.what = URL_ERROR;
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					mes.what = NETWORK_ERROR;
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mes.what = JSON_ERROR;
				} finally {
					// 停留在页面2s的时间
					long endTime = System.currentTimeMillis();// 记录结束时间
					// 我们花了多少时间
					long dTime = endTime - startTime;
					// 2000
					if (dTime < 2000) {
						try {
							Thread.sleep(2000 - dTime);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					handler.sendMessage(mes);
				}

			};
		}.start();

	}

	protected void enterHome() {
		Intent intent = new Intent(this, HomeActivity.class);
		startActivity(intent);
		// 关闭当前页面
		finish();

	}

	/**
	 * 弹出升级对话框
	 */
	private void showUpdateDialog() {
		//this = Activity.this
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle("提示升级");
//		builder.setCancelable(false);//强制升级
		//升级 点击其他地方 捕捉
		builder.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				enterHome();
				dialog.dismiss();
			}
		});
		builder.setMessage(description);
		builder.setPositiveButton("立刻升级", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//下载 APK  并且替换安装
				//判断SD卡是否存在
				if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
					//sdcard 存在
					//afinal的使用
					FinalHttp finalhttp = new FinalHttp();
					finalhttp.download(apkurl, 
							Environment.getExternalStorageDirectory().getAbsolutePath()+"/MobileSafe.apk",
							new AjaxCallBack<File>() {
								
								//下载失败
								@Override
								public void onFailure(Throwable t, int errorNo,
										String strMsg) {
									Toast.makeText(getApplicationContext(), "下载失败", 0).show();
									super.onFailure(t, errorNo, strMsg);
								}
								//正在下载
								/**
								 * count  文件总大小
								 * current 大概的当前位置
								 */
								@Override
								public void onLoading(long count, long current) {
									super.onLoading(count, current);
									tv_update_info.setVisibility(View.VISIBLE);//设置显示
									int progress = (int)(current*100/count);
									tv_update_info.setText("下载进度:" +progress+"%");
									Toast.makeText(getApplicationContext(), "下载100", 0).show();
								}

								@Override
								public void onSuccess(File t) {
									super.onSuccess(t);
									//安装apk文件  t文件路径
									installAPK(t);
								}
								//安装apk文件
								private void installAPK(File t) {
									Intent intent = new Intent();
									intent.setAction("android.intent.action.VIEW");//设置动作 安装APK工具的意图的name
									intent.addCategory("android.intent.category.DEFAULT");
									intent.setDataAndType(Uri.fromFile(t), "application/vnd.android.package-archive");
									startActivity(intent);
								}
						
							}
							);
				}else{
					Toast.makeText(getApplicationContext(), "没有SD卡 请安装后再试", 0).show();
				}
				
			}
		});
		builder.setNegativeButton("下次再说", new  OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				 dialog.dismiss();//让对话框  消失
				 enterHome();//进入主页面
			}
		});
		builder.show();
	}

	private String getVersionName() {
		// 用来管理手机的APK
		PackageManager pm = getPackageManager();

		try {
			// 得到知道APK的功能清单文件
			PackageInfo info = pm.getPackageInfo(getPackageName(), 0);
			return info.versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}

}
