package com.example.mobilesafa;

import com.example.mobilesafa.utils.MD5Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity {

	protected static final String TAG = "HomeActivity";
	private GridView list_home;
	private MyAdapter adapter;

	private SharedPreferences sp;
	private static String[] names = { " 手机防盗", "通信卫士", "软件管理", "进程管理", "流量统计",
			"手机杀毒", "缓存清理", "高级工具", "设置中心" };
	private static int[] ids = { R.drawable.safe, R.drawable.callmsgsafe,
			R.drawable.app, R.drawable.taskmanager, R.drawable.netmanager,
			R.drawable.trojan, R.drawable.sysoptimize, R.drawable.atools,
			R.drawable.settings };
	private EditText et_setup_pwd;
	private EditText et_setup_confirm;
	private Button ok;
	private Button cancel;
	private AlertDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		sp = this.getSharedPreferences("config", MODE_PRIVATE);
		list_home = (GridView) findViewById(R.id.list_home);// 宫格和 listView
															// 是继承一个父类
		adapter = new MyAdapter();// 實例化adapter
		list_home.setAdapter(adapter);// adapter 接口 实现接口的实例类
		list_home.setOnItemClickListener(new OnItemClickListener(

		) {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position) {
				case 8:// 进入设置中心
					Intent intent = new Intent(HomeActivity.this,
							SettingActivity.class);
					startActivity(intent);
					break;
				case 0:// 进入手机防盗页面
					showLostFindDialog();
					break;
				default:
					break;
				}
			}
		});
	}

	protected void showLostFindDialog() {
		// 判断是否设置过密码
		if (isSetupPwd()) {
			// 已经设置密码,弹出的是输入对话框
			showEnterDialog();
		} else {
			// 没有设置密码 .弹出设置密码的对话框
			showSetupPwdDialog();
		}

	}

	/**
	 * 设置密码对话框
	 */
	private void showSetupPwdDialog() {
		AlertDialog.Builder builder = new Builder(HomeActivity.this);
		// 自定义一个对话框布局文件
		View view = View.inflate(HomeActivity.this,
				R.layout.dialog_setup_password, null);
		
		et_setup_pwd = (EditText) view.findViewById(R.id.et_setup_pwd);
		et_setup_confirm = (EditText) view.findViewById(R.id.et_setup_confirm);
		ok = (Button) view.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//取出密码
				String passwd = et_setup_pwd.getText().toString().trim();
				String passwd_confirm = et_setup_confirm.getText().toString().trim();
				if(TextUtils.isEmpty(passwd)||TextUtils.isEmpty(passwd_confirm)){
					Toast.makeText(HomeActivity.this, "密码为空", 0).show();
					return;
				}
				if(passwd.equals(passwd_confirm)){
					//一致的话就保存,吧对话框取消掉,还要进入手机防盗页面
					Editor editor = sp.edit();
					editor.putString("passwd", MD5Utils.md5Passwd(passwd));
					editor.commit();
					dialog.dismiss();
					Log.i(TAG, "进入手机防盗页面");
					Intent intent = new Intent(HomeActivity.this,LostFindActivity.class);
					startActivity(intent);
					
					
				}else{
					Toast.makeText(HomeActivity.this, "密码不一致", 0).show();
					return;
				}
			}
		});
		cancel = (Button) view.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 把这个对话框取消掉 
			dialog.dismiss();
				Log.i(TAG, "取消");
				
			}
		});
		
		dialog = builder.create();
		dialog.setView(view, 0, 0, 0, 0);
		dialog.show();
	}

	/**
	 * 输入密码对话框
	 */
	private void showEnterDialog() {
		AlertDialog.Builder builder = new Builder(HomeActivity.this);
		// 自定义一个对话框布局文件
		View view = View.inflate(HomeActivity.this,
				R.layout.dialog_enter_password, null);
		
		et_setup_pwd = (EditText) view.findViewById(R.id.et_setup_pwd);
		ok = (Button) view.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//取出密码
				String passwd = et_setup_pwd.getText().toString().trim();
				String savePasswd = sp.getString("passwd", null);//取出加密后的
				if(TextUtils.isEmpty(passwd)){
					Toast.makeText(HomeActivity.this, "密码为空", 0).show();
					return ;
				}
				if(MD5Utils.md5Passwd(passwd).equals(savePasswd)){
					//密码是我之前设置的密码 
					//把对话框消掉,进入主页面
					dialog.dismiss();
					Log.i(TAG,"进入主页面");
					Intent intent = new Intent(HomeActivity.this,LostFindActivity.class);
					startActivity(intent);
				}else{
					//密码不一致
					et_setup_pwd.setText("");
					Toast.makeText(HomeActivity.this, "密码不一致", 0).show();
					return ;
				}
			}
		});
		cancel = (Button) view.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 把这个对话框取消掉 
			dialog.dismiss();
				Log.i(TAG, "取消");
				
			}
		});
		dialog = builder.create();
		dialog.setView(view, 0, 0, 0, 0);
		dialog.show();
	}

	/**
	 * 判断是否设置过密码
	 * 
	 * @return
	 */
	private boolean isSetupPwd() {
		String passwd = sp.getString("passwd", null);
		if (TextUtils.isEmpty(passwd)) {// 判断passwd 是否为空
			return false;
		} else {
			return true;
		}
		// return !TextUtils.isEmpty(passwd);
	}

	private class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			// 返回GridView 总个数
			return names.length;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View view = View.inflate(HomeActivity.this,
					R.layout.list_item_home, null);
			ImageView iv_item = (ImageView) view.findViewById(R.id.iv_item);
			TextView tv_item = (TextView) view.findViewById(R.id.tv_item);
			iv_item.setImageResource(ids[position]);
			tv_item.setText(names[position]);
			return view;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

}
