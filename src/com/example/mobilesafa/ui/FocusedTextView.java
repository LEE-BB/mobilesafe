package com.example.mobilesafa.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.ExportedProperty;
import android.widget.TextView;
/**
 * 自定义 一个ＴｅｘｔＶｉｅｗ 一出生就有焦点
 * @author BB_LEE
 *
 */
public class FocusedTextView extends TextView {

	public FocusedTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public FocusedTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public FocusedTextView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 当前并没有焦点  只是欺骗 了 Android系统
	 */
	@Override
	@ExportedProperty(category = "focus")
	public boolean isFocused() {
		//获得焦点  返回true  
		return true;
	}

}
