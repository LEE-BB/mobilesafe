package com.example.mobilesafa.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamTools {
	/**
	 * @param is 输入流
	 * @return String 返回的字符串
	 * @throws IOException 
	 * 
	 */
	public static String readFromStream(InputStream is) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();//baos输出流 写出
		byte[] buffer = new byte[1024];// buffer 1024字节 缓冲区
		int len = 0;
		while((len = is.read(buffer))!=-1){//从is读入保存到buffer缓冲区
			baos.write(buffer, 0, len);// 因为inputStream里面的内容你不知道具体有多长，
//										所以无法确定到底buffer需要多长1024也许未必够用，所以只能把buffer当做缓存，
//										每次读进一部分，在把buffer的内容，写到足够大的内存区。
//										如果你确定你的输入流中字节数小于1024，你也可以直接返回buffer
		}
		is.close();
		String result = baos.toString();
		baos.close();
		return result;
	}
}
